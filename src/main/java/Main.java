import util.DataSourceProvider;
import util.DbUtil;
import util.FileUtil;
import util.PropertyLoader;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by Kodu on 15/02/2017.
 */
public class Main {
    public static void main(String[] args) {

        String url = PropertyLoader.getProperty("jdbc.url");
        DataSourceProvider.setDbUrl(url);
        String schema = FileUtil.readFileFromClasspath("schema.sql");

        try(Connection c = DataSourceProvider.getDataSource().getConnection()){
            DbUtil.insertFromString(c, schema);
        }catch(SQLException e){
            throw new RuntimeException(e);
        }

    }
}
